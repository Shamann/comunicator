package application;

import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import logical.Configurator;
import logical.Connector;

public class ChatController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		myipLbl.setText(Configurator.getSingleton().getMyIP());
		portLbl.setText(Configurator.getPort());
		
		Task<LinkedList<Text>> task = chat.receiveMessage();
		task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				communication.getChildren().addAll(task.getValue());
			}
		});

		Thread receiver = new Thread(task);
		receiver.setDaemon(true);
		receiver.start();
	}
	
	@FXML
	private Label portLbl;
	@FXML
	private Label myipLbl;
	@FXML
	private TextField ip1;
	@FXML
	private TextField ip2;
	@FXML
	private TextField ip3;
	@FXML
	private TextField ip4;
	@FXML
	private TextField lengthFld;
	@FXML
	private Slider slider;
	@FXML
	private TextArea newMessage;
	@FXML
	private TextFlow communication;
	@FXML
	private Label warningText;
	@FXML
	private Button send;
	@FXML
	private Button changer;
	
	private Connector chat = new Connector();
	
	@FXML
	public void sendMessage() {
		String message = newMessage.getText();
		if (ip1.getText().isEmpty() || ip2.getText().isEmpty() || ip3.getText().isEmpty() || ip4.getText().isEmpty()) {
			return;
		}
		String targetIp = ip1.getText() + "."
						+ ip2.getText() + "." 
						+ ip3.getText() + "."
						+ ip4.getText();
		
		if (!chat.isTargetIpSet()) {
			chat.setTargetIp(targetIp);
		} else 
		if (!targetIp.equalsIgnoreCase(chat.getTargetIP())) {
			warningText.setVisible(true);
			String[] ips = chat.getTargetIP().split("\\.");
			ip1.setText(ips[0]);
			ip2.setText(ips[1]);
			ip3.setText(ips[2]);
			ip4.setText(ips[3]);
		}
		communication.getChildren().add(chat.sendMessage(message, (int) slider.getValue()));
	}
	
	public static LinkedList<Text> receivedMessages = new LinkedList<Text>();
	
	@FXML
	public void receiveMessage() {
		if(send.disableProperty().get()) {
			send.setDisable(false);
			changer.setText("Receiver");
			return;
		} else {
			send.setDisable(true);
			changer.setText("Sender");
			while (true) {
				communication.getChildren().addAll(receivedMessages);
				receivedMessages.clear();
				if (receivedMessages.isEmpty()) {
					return;
				}
			}
		}
	}
	
	@FXML
	public void setLengthField(MouseEvent event) {
		int val = (int) slider.getValue();
		lengthFld.setText(Integer.toString(val));
	}
	
	@FXML
	public void setSlider(KeyEvent event) {
		if (!"0123456789".contains(event.getCharacter())) {
			event.consume();
		} else {
			slider.setValue(Double.valueOf(lengthFld.getText()+event.getCharacter()));
			System.out.println(slider.getValue());
		}
	}
	
	@FXML
	public void shutDown() {
		chat.sendMessage("end00FXend", (int)slider.getValue());
		System.exit(0);
	}
	@FXML
	public void showAbout() {
		
	}
	
}
