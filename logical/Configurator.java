package logical;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Configurator {
	public static final int port = 28050;
	private static String myIP;
	private static InetAddress myInetApiIP;
	
	private static final Configurator singleton = new Configurator();
	
	private Configurator() {
		try {
			myIP = Inet4Address.getLocalHost().getHostAddress();
			myInetApiIP = Inet4Address.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	public InetAddress getInetApiIP() {
		return myInetApiIP;
	}
	
	public static Configurator getSingleton() {
		return singleton;
	}
	
	public String getMyIP() {
		return myIP;
	}
	public static String getPort() {
		return Integer.toString(port);
	}

}
