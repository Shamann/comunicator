package logical;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedList;

import application.ChatController;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;


public class Connector {

	class ChatProtocol {
		
		private byte[] fragmentLength = new byte[2];
		private byte[] numberOfFragments = new byte[4];
		private byte[] sequenceNumber = new byte[4];
		private byte[] flags = new byte[2];
		
		/**
		 * Creates Header of chat protocol
		 * @param length
		 * @param numberOfFragments
		 * @param sequence
		 * @param flag
		 */
		public ChatProtocol(int length, int numberOfFragments, int sequence, int flag) {
			fragmentLength[1] = (byte) (length&0xFF);
			fragmentLength[0] = (byte) ((length>>8)&0xFF);
			
			this.numberOfFragments[3] = (byte) (numberOfFragments&0xFF);
			this.numberOfFragments[2] = (byte) ((numberOfFragments>>8)&0xFF);
			this.numberOfFragments[1] = (byte) ((numberOfFragments>>16)&0xFF);
			this.numberOfFragments[0] = (byte) ((numberOfFragments>>24)&0xFF);
			
			this.sequenceNumber[3] = (byte) (sequence&0xFF);
			this.sequenceNumber[2] = (byte) ((sequence>>8)&0xFF);
			this.sequenceNumber[1] = (byte) ((sequence>>16)&0xFF);
			this.sequenceNumber[0] = (byte) ((sequence>>24)&0xFF);
			
			flags[1] = (byte) (flag&0xFF);
			flags[0] = (byte) ((flag>>8)&0xFF);
		}
		/**
		 * Creates byte array of Chat protocol header<br>
		 * Protocol values in order of bytes: <br>
		 * <ul>
		 * <li>Flags (2B)</li>
		 * <li>Frame length (2B)</li>
		 * <li>Number of Fragments (4B)</li>
		 * <li>Sequence number of datagram (4B)</li>
		 * </ul>
		 * @return byte[]
		 */
		public byte[] getHeader() {
			byte[] output = new byte[12];
			ByteBuffer header = ByteBuffer.wrap(output);
			
			header.put(flags);
			header.put(fragmentLength);
			header.put(numberOfFragments);
			header.put(sequenceNumber);
			
			return output;
		}
		public int getHeaderLength() {
			return 12;
		}
	}
	

	private InetAddress targetIP;
	private DatagramSocket socket;
	
	public Connector() {
		try {
			socket = new DatagramSocket(Configurator.port, Configurator.getSingleton().getInetApiIP());
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isTargetIpSet() {
		return targetIP == null? false : true;
	}
	
	public void setTargetIp(String targetIP) {
		try {
			this.targetIP = Inet4Address.getByName(targetIP);
		} catch (UnknownHostException e) {
			System.out.println("Host name is not recognised as valid IP address");
			e.printStackTrace();
		} finally {
			System.out.println(this.targetIP.getHostAddress());
		}
	}
	
	public String getTargetIP() {
		return targetIP.getHostAddress();
	}
	
	public Text sendMessage(final String message, int fragmentLength) {
		
		int m_length = message.length();
		
		if (message.equalsIgnoreCase("end00FXend")) {
			ChatProtocol header = new ChatProtocol(m_length, 1, 1, 1);
		}
		if (m_length>1400 || fragmentLength<1400) {
			Task<Void> sender = new Task<Void>() {
				@Override
				protected Void call() throws Exception {
					LinkedList<byte[]> fragmentedMessage = new LinkedList<byte[]>();
					byte[] byteMsg = message.getBytes();
					int noaf = m_length / fragmentLength;
					noaf += m_length % fragmentLength > 0 ? 1 : 0;
					
					for (int i = 0, j = 1; i < m_length; i+=fragmentLength, j++) {
						if (i+fragmentLength > byteMsg.length) {
							ChatProtocol header = new ChatProtocol(m_length-i, noaf, j, 0);
							byte[] tmp = Arrays.copyOfRange(byteMsg, i, m_length);
							byte[] msgFull = new byte[tmp.length + header.getHeaderLength()];
							ByteBuffer hlp = ByteBuffer.wrap(msgFull);
							hlp.put(header.getHeader());
							hlp.put(tmp);
							fragmentedMessage.add(msgFull);
						} else {
							ChatProtocol header = new ChatProtocol(fragmentLength, noaf, j, 0);
							byte[] tmp = Arrays.copyOfRange(byteMsg, i, i + fragmentLength);
							byte[] msgFull = new byte[tmp.length + header.getHeaderLength()];
							ByteBuffer hlp = ByteBuffer.wrap(msgFull);
							hlp.put(header.getHeader());
							hlp.put(tmp);
							fragmentedMessage.add(msgFull);
						}
					}
					for (byte[] msg : fragmentedMessage) {
						DatagramPacket outSend = new DatagramPacket(msg, msg.length, targetIP, Configurator.port);
						socket.send(outSend);
					}
					return null;
				}
				
			};
			sender.setOnFailed(new EventHandler<WorkerStateEvent>() {
				@Override
				public void handle(WorkerStateEvent event) {
					sender.getException().printStackTrace();
				}
			});
			new Thread(sender).start();
			
		} else {
			ChatProtocol header = new ChatProtocol(m_length, 1, 1, 0);
			int headerLength = header.getHeaderLength();
			// FIXME message length by UTF-8 encoding
			byte[] msg = new byte[headerLength + m_length];
			ByteBuffer tmp = ByteBuffer.wrap(msg);
			tmp.put(header.getHeader());
			tmp.put(message.getBytes());
			
			DatagramPacket outSend = new DatagramPacket(msg, msg.length, targetIP, Configurator.port);
			try {
				socket.send(outSend);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		String newMessage = "Me: " + Configurator.getSingleton().getMyIP() + "\n" + message + "\n";
		Text sentMessage = new Text(newMessage);
		sentMessage.setStyle("-fx-fill: blue; -fx-font-weight: bold;");
		return sentMessage;
	}
	
	
	public Task<LinkedList<Text>> receiveMessage() {
		byte[] buffer = new byte[1400];
		DatagramPacket received = new DatagramPacket(buffer, buffer.length);
		Task<LinkedList<Text>> receiver = new Task<LinkedList<Text>>(){

			@Override
			protected LinkedList<Text> call() throws Exception {
				LinkedList<Text> messages = new LinkedList<Text>();
				while (true) {
					try {
						socket.receive(received);
						String message = "IP: " + received.getAddress().getHostAddress();
						byte[] data = received.getData();
						int flength = ((data[2]&0xFF)<<8) + ((data[3]&0xFF));
						byte[] text = Arrays.copyOfRange(data, 12, 12+flength);
						int flag = data[1]&0x1;
						int noaf = ((data[4]&0xFF)<<24) + ((data[5]&0xFF)<<16) + ((data[6]&0xFF)<<8) + ((data[7]&0xFF));
						int sn = ((data[8]&0xFF)<<24) + ((data[9]&0xFF)<<16) + ((data[10]&0xFF)<<8) + ((data[11]&0xFF));
						
						if (noaf == 1) {
							String msg = new String(text, "UTF-8");
							message += " « Fragments: " + noaf + "\n";
							message += msg + "\n";
							Text receivedMessage = new Text(message);
							receivedMessage.setStyle("-fx-fill: orangered; -fx-font-weight: bold;");
							ChatController.receivedMessages.add(receivedMessage);
							if (flag==1) {
								ChatController.receivedMessages.add(new Text("End of conversation\n"));
								break;
							}
						} else {
							String msg = new String (text, "UTF-8");
							message += " « Fragments: " + noaf + "\n";
							message += msg;
							int i=1;
							while (i < noaf) {
								DatagramPacket fragmented = new DatagramPacket(buffer, buffer.length);
								socket.receive(fragmented);
								data = received.getData();
								flength = ((data[2]&0xFF)<<8) + ((data[3]&0xFF));
								text = Arrays.copyOfRange(data, 12, 12+flength);
								flag = data[1]&0x1;
								sn = ((data[8]&0xFF)<<24) + ((data[9]&0xFF)<<16) + ((data[10]&0xFF)<<8) + ((data[11]&0xFF));
								String fragment = new String(text);
								i++;
								message += fragment;
							}
							message += "\n";
							Text receivedMessage = new Text(message);
							receivedMessage.setStyle("-fx-fill: orangered; -fx-font-weight: bold;");
							ChatController.receivedMessages.add(receivedMessage);
							if (flag==1) {
								ChatController.receivedMessages.add(new Text("End of conversation\n"));
								break;
							}
						}
						
					} catch (IOException e) {
						e.printStackTrace();
					} 
				}
				return messages;
			}
		};
		return receiver;
	}
}


